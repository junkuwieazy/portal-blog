/*
SQLyog Enterprise v10.42 
MySQL - 5.5.5-10.1.25-MariaDB : Database - blog_project
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`blog_project` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `blog_project`;

/*Table structure for table `ads_tb` */

DROP TABLE IF EXISTS `ads_tb`;

CREATE TABLE `ads_tb` (
  `id_ad` int(11) NOT NULL AUTO_INCREMENT,
  `ad_title` varchar(250) DEFAULT NULL,
  `advertiser` varchar(250) DEFAULT NULL,
  `ad_postition` varchar(250) DEFAULT NULL,
  `ad_width` int(11) DEFAULT NULL,
  `ad_height` int(11) DEFAULT NULL,
  `ad_path` text,
  `ad_datetime` datetime DEFAULT NULL,
  `ad_status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_ad`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `ads_tb` */

insert  into `ads_tb`(`id_ad`,`ad_title`,`advertiser`,`ad_postition`,`ad_width`,`ad_height`,`ad_path`,`ad_datetime`,`ad_status`) values (1,'Denis Gendur','Jati Juga Gendur',NULL,1000,1000,'uploads/ads/banner/20180126164225_37238.jpeg','2018-01-26 16:42:25','2'),(2,'Denis Gendur','Jati Juga Gendur',NULL,1000,1000,'uploads/ads/banner/20180126164247_55942.jpeg','2018-01-26 16:42:47','2'),(3,'Denis Gendur','Jati Juga Gendur',NULL,1000,1000,'uploads/ads/banner/20180126164307_38541.jpeg','2018-01-26 16:43:07','2'),(4,'Denis Gendut','Jati Juga Gendut',NULL,1000,1000,'uploads/ads/banner/20180126165304_18293.jpeg','2018-01-26 16:53:04','2'),(5,'dddddddddd','ccccccccccccc','1',1000,1000,'uploads/ads/banner/20180126170842_80639.jpeg','2018-01-26 17:08:42','2'),(6,'aaaa','bbbb','1',1000,1000,'uploads/2018/January/ads/banner/aaaa_20180129151140_87420.jpeg','2018-01-29 15:11:40','2');

/*Table structure for table `album_gallery_tb` */

DROP TABLE IF EXISTS `album_gallery_tb`;

CREATE TABLE `album_gallery_tb` (
  `id_album` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_gallery` int(11) DEFAULT NULL,
  `album_name` varchar(250) DEFAULT NULL,
  `album_thumbnail` text,
  `datetime_album` datetime DEFAULT NULL,
  PRIMARY KEY (`id_album`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `album_gallery_tb` */

/*Table structure for table `avatar_tb` */

DROP TABLE IF EXISTS `avatar_tb`;

CREATE TABLE `avatar_tb` (
  `id_avatar` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `avatar_path` text,
  `avatar_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id_avatar`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `avatar_tb` */

insert  into `avatar_tb`(`id_avatar`,`id_user`,`avatar_path`,`avatar_datetime`) values (1,9,'/uploads/avatar/8cdc2cab118fd6423915aa9f19556424_thumb.jpeg','2018-02-07 15:15:33'),(2,9,'/uploads/avatar/8cdc2cab118fd6423915aa9f19556424_thumb.jpeg','2018-02-07 15:26:42'),(3,9,'/uploads/avatar/8cdc2cab118fd6423915aa9f19556424_thumb.jpeg','2018-02-07 15:28:20'),(4,19,'/uploads/avatar/46dc4a46337e08e1a8fe800dfc160180_thumb.jpeg','2018-02-07 16:04:18'),(5,19,'/uploads/avatar/46dc4a46337e08e1a8fe800dfc160180_thumb.jpeg','2018-02-08 09:33:54'),(6,19,'/uploads/avatar/46dc4a46337e08e1a8fe800dfc160180_thumb.jpeg','2018-02-08 09:34:05'),(7,19,'/uploads/avatar/46dc4a46337e08e1a8fe800dfc160180_thumb.jpeg','2018-02-08 09:35:40'),(8,19,'/uploads/avatar/46dc4a46337e08e1a8fe800dfc160180_thumb.jpeg','2018-02-08 09:35:50'),(9,19,'/uploads/avatar/46dc4a46337e08e1a8fe800dfc160180_thumb.jpeg','2018-02-08 09:38:54'),(10,20,'/uploads/avatar/31bbb38b4a72fbc8eae96bdc4165279f_thumb.jpeg','2018-02-08 09:55:58');

/*Table structure for table `bounce_tb` */

DROP TABLE IF EXISTS `bounce_tb`;

CREATE TABLE `bounce_tb` (
  `id_bounce` int(11) NOT NULL AUTO_INCREMENT,
  `random_page_id` varchar(100) DEFAULT NULL,
  `id_content` int(11) DEFAULT NULL,
  `ip_client` varchar(100) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `bounce_datetime` datetime DEFAULT NULL,
  `bounce_sc` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_bounce`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

/*Data for the table `bounce_tb` */

insert  into `bounce_tb`(`id_bounce`,`random_page_id`,`id_content`,`ip_client`,`id_user`,`bounce_datetime`,`bounce_sc`) values (9,'037287080949441154',1,'::1',0,'2018-02-12 11:58:12',50),(10,'023271259935644895',1,'::1',0,'2018-02-12 12:00:55',50),(11,'05842981771940421',1,'::1',0,'2018-02-12 12:02:19',30),(12,'03200277327445673',1,'::1',0,'2018-02-12 12:03:37',100),(13,'04067486196403942',1,'::1',0,'2018-02-12 12:05:46',50),(14,'04865001150081021',1,'::1',0,'2018-02-12 12:07:03',40),(15,'09579304608606765',1,'::1',0,'2018-02-12 12:08:14',10),(16,'006602347416387477',1,'::1',0,'2018-02-12 12:09:00',90),(17,'07442560328860273',1,'::1',0,'2018-02-12 12:10:57',130),(18,'06049994821896478',1,'::1',0,'2018-02-12 12:13:39',10),(19,'03002975315650189',1,'::1',0,'2018-02-12 12:14:21',830),(20,'017824116983129468',1,'::1',0,'2018-02-12 12:29:00',40),(21,'08889460775735392',1,'::1',0,'2018-02-12 12:30:08',10),(22,'038837771151525247',1,'::1',0,'2018-02-12 12:31:06',530),(23,'06215905851248593',1,'::1',0,'2018-02-12 12:40:25',50),(24,'09714143679493912',1,'::1',0,'2018-02-12 12:41:47',10),(25,'006817425961787671',1,'::1',0,'2018-02-12 12:42:25',120),(26,'08872227977740745',1,'::1',0,'2018-02-12 12:45:20',70),(27,'07632288475865832',1,'::1',0,'2018-02-12 12:48:06',40),(28,'05493154337401129',1,'::1',0,'2018-02-12 12:49:29',230),(29,'03090335037695364',1,'::1',0,'2018-02-12 12:54:25',80),(30,'02244283090943502',1,'::1',0,'2018-02-12 12:56:22',50),(31,'04844858995195942',1,'::1',0,'2018-02-12 12:58:05',40),(32,'03419620709771054',1,'::1',0,'2018-02-12 12:59:18',310),(33,'01519592286038851',1,'::1',0,'2018-02-12 13:05:59',70),(34,'036854911687625647',1,'::1',0,'2018-02-12 13:07:41',70),(35,'018243988984331438',1,'::1',0,'2018-02-12 13:09:35',110),(36,'019318642526868346',1,'::1',0,'2018-02-12 13:11:51',NULL),(37,'09553604322835294',1,'::1',0,'2018-02-12 13:12:24',30),(38,'013129995770246894',1,'::1',0,'2018-02-12 13:13:21',NULL),(39,'023885336086969122',2,'::1',0,'2018-02-12 13:14:56',70),(40,'01451091521212835',2,'::1',0,'2018-02-12 13:19:13',120),(41,'08321320846111651',3,'::1',0,'2018-02-12 13:21:54',60),(42,'05250583550432224',3,'::1',0,'2018-02-12 13:23:26',40),(43,'022722484233178664',3,'::1',0,'2018-02-12 13:24:35',NULL),(44,'05893680390927163',3,'::1',0,'2018-02-12 13:25:09',30),(45,'009132047454421732',3,'::1',0,'2018-02-12 13:26:33',40),(46,'07153749032218408',3,'::1',0,'2018-02-12 14:58:08',60);

/*Table structure for table `comment_content_tb` */

DROP TABLE IF EXISTS `comment_content_tb`;

CREATE TABLE `comment_content_tb` (
  `id_comment` int(11) NOT NULL AUTO_INCREMENT,
  `id_content` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `comment` text,
  `ip_client` varchar(200) DEFAULT NULL,
  `comment_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id_comment`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `comment_content_tb` */

insert  into `comment_content_tb`(`id_comment`,`id_content`,`id_user`,`comment`,`ip_client`,`comment_datetime`) values (2,1,9,'asasasas','192.168.1.102','2018-02-08 13:53:59'),(3,2,9,'test comment','192.168.1.102','2018-02-08 14:19:56'),(4,2,9,'\\asas','192.168.1.102','2018-02-08 16:28:25'),(5,2,9,'asasas','192.168.1.102','2018-02-08 16:29:13'),(6,1,9,'sldfu','192.168.1.102','2018-02-08 17:10:54');

/*Table structure for table `content_gallery_tb` */

DROP TABLE IF EXISTS `content_gallery_tb`;

CREATE TABLE `content_gallery_tb` (
  `id_content` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_category` int(11) DEFAULT NULL,
  `id_album` int(11) DEFAULT NULL,
  `kind` varchar(10) DEFAULT NULL,
  `title_gallery` varchar(250) DEFAULT NULL,
  `content_describ` longtext,
  `content_link` text,
  `path_gallery` text,
  `datetime_gallery` datetime DEFAULT NULL,
  `edited_status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_content`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `content_gallery_tb` */

/*Table structure for table `content_reject_tb` */

DROP TABLE IF EXISTS `content_reject_tb`;

CREATE TABLE `content_reject_tb` (
  `id_reject` int(11) NOT NULL AUTO_INCREMENT,
  `id_content` int(11) DEFAULT NULL,
  `reason_rejected` text,
  `datetime_reject` datetime DEFAULT NULL,
  `status_reject` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_reject`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `content_reject_tb` */

insert  into `content_reject_tb`(`id_reject`,`id_content`,`reason_rejected`,`datetime_reject`,`status_reject`) values (1,9,'ljllhj\r\n','2018-01-25 11:39:47','1'),(2,9,'isydsiydg sduhsady','2018-01-25 11:40:40','1'),(3,8,'asas asasASahsaJAjsa','2018-01-25 11:40:53','1'),(4,24,'sds','2018-01-25 12:01:30','1'),(5,24,'waiting\r\n','2018-01-25 12:02:51','1'),(6,24,'asasas','2018-01-25 12:05:14','1'),(7,24,'fdgdhtdhdt','2018-01-25 12:30:02','1'),(8,24,'rrrrrr','2018-01-25 12:30:02',NULL),(9,24,'susah dimengerti','2018-01-25 13:04:58','0'),(10,26,'jelek','2018-01-29 10:27:32','0'),(11,27,'banget','2018-01-29 10:28:35','1'),(12,26,'garuk garuk','2018-01-29 10:29:06','0'),(13,26,'bisa gitu ya','2018-01-29 10:30:11','0'),(14,26,'asasasas','2018-01-29 10:52:40','1');

/*Table structure for table `content_tb` */

DROP TABLE IF EXISTS `content_tb`;

CREATE TABLE `content_tb` (
  `id_content` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_category` int(11) DEFAULT NULL,
  `id_sub` int(11) DEFAULT NULL,
  `kind` varchar(10) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `content_describ` longtext,
  `content_video` text,
  `content_thumbnail` text,
  `content_link` text,
  `content_datetime` datetime DEFAULT NULL,
  `edited_status` varchar(10) DEFAULT NULL,
  `content_status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_content`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `content_tb` */

insert  into `content_tb`(`id_content`,`id_user`,`id_category`,`id_sub`,`kind`,`title`,`content_describ`,`content_video`,`content_thumbnail`,`content_link`,`content_datetime`,`edited_status`,`content_status`) values (1,9,14,54,'1','Lazio Gagal Perlebar Jarak, Juventus Dan Napoli Bersiang Ketat','<p>Dilansir dari legaseriea.it (06/02/2018), Liga Italia Serie A telah memainkan pekan ke-23 Liga Italia Serie-A Pekan ini. Dini hari tadi laga tuan rumah Lazio menjamu tamunya Genoa berakhir dengan hasil kemenangan tim tamu dengan skor 1-2. Hal ini membuat pasukan Biancocelesti gagal memperlebar jarak mereka dari kompetitor dibawahnya Inter Milan dan AS Roma. Berikut hasil 10 pertandingan Serie-A pekan ke-23.</p><p><br></p><p>Sampdoria vs Torino 1-1 (Lucas Torreira 16&#39;; Afriyie Acquah 25&#39;)<br>Inter Milan vs Crotone 1-1 (Eder 23&#39;; Andrea Barberis 60&#39;)<br>Hellas Verona vs Roma 0-1 (Cengiz Under 1&#39;)<br>Atalanta vs ChievoVerona 1-0 (Gianluca Mancini 72&#39;)<br>Bologna vs Fiorentina 1-2 (Erick Pulgar 44&#39;; Jordan Veretout 41&#39;, Federico Chiesa 71&#39;)<br>Cagliari vs SPAL2013 2-0 (Luca Cigarini 34&#39;, Marco Sau 78&#39;)<br>Juventus vs Sassuolo 7-0 (Alex Sandro 9&#39;, Sami Khedira 24&#39;,27&#39;, miralem Pjanic 38&#39;, Gonzalo Higuain 63&#39;,74&#39;,83&#39;)</p><p><br></p><p>Udinese vs AC Milan 1-1 (Gianluigi Donnarumma 76&#39;(og); Suso 9&#39;)<br>Benevento vs Napoli 0-2 (Dries Mertens 20&#39;, Marek Hamsik 47&#39;)<br>Lazio vs Genoa 1-2 (Marco Parolo 59&#39;; Goran Pandev 55&#39;, Diego Laxalt 90&#39;)<br><br>Kemenangan Juventus dan Napoli dipekan ini membuat serie-A semakin menarik, pasalnya kedua tim kini hanya berselisih satu poin. Napoli dengan 60poin sementara Juventus dengan 59poin, kedua tim tampil konsisten dengan sama sama mengoleksi 19kemenangan.</p>','','/uploads/2018/February/thumbnails/article_thumbnail/lazio_gagal_perlebar_jarak__juventus_dan_napoli_bersiang_ketat__update_klasemen_serie_a_20180206110738_62802_thumb.jpeg','lazio_gagal_perlebar_jarak__juventus_dan_napoli_bersiang_ketat__update_klasemen_serie_a_20180206110738','2018-02-06 11:07:38','0','1'),(2,9,11,20,'1','Akhir Perjuangan Dianti','<p>Karyawati Garuda Maintenance Facility (GMF) Aero Asia, Dianti Dyah Ayu Cahyani Putri (24) meninggal di Rumah Sakit Mayapada, Tangerang setelah sempat bertahan selama belasan jam usai mobil Honda Brio miliknya tertimbun longsor di jalan Perimeter Selatan, Bandara Soekarno-Hatta.<br><br>Mobil Honda Brio bernomor polisi A 1567 AS itu melintas lokasi kejadian di Jalan Perimeter, Senin (5/2) sekitar pukul 17.00 WIB.<br><br>Menurut Direktur Keuangan Garuda Maintenance Facility (GMF) Aeroasia, Insan Nurcahyo, jalan yang dilalui keduanya merupakan jalan yang biasa dilalui setelah pulang kerja.<br><br>Dianti merupakan pegawai tetap GMF, sedangkan Mutmainah berstatus pegawai outsourcing.<br><br>&quot;Jam kerja pukul 07.00-16.00 WIB,&quot; kata Insan.<br><br>Saat kejadian, kondisi sedang hujan. Dinding underpass roboh, dan langsung menimpa mobil keduanya.<br><br>VP Corporate Secretary PT GMF AeroAsia M. Arif Faisal menjelaskan, saat itu, mobil dikendarai oleh Dianti bersama rekannya Mutmainah Syamsuddin.</p>','','/uploads/2018/February/thumbnails/article_thumbnail/akhir_perjuangan_dianti_20180206111329_27197_thumb.jpeg','akhir_perjuangan_dianti_20180206111329','2018-02-06 11:13:29','0','1'),(3,20,16,60,'1','Ceritaku hari ini','<p>Hari ini saya pergi bekerja. Saat akan berangkat menuju tempat bekerja, hujan turun dengan derasnya. Agar tidak basah saat tiba di tempat bekerja, maka saya memutuskan untuk menggunakan jas hujan. Begitu selesai menggunakan jas hujan dan saya sudah siap untuk berangkat bekerja, tiba - tiba hujan berhenti. Tapi tidak mengapa, saya ikhlas heeeeeeeeeheeeeeee</p>','','/uploads/2018/February/thumbnails/article_thumbnail/ceritaku_hari_ini_20180208100644_7090_thumb.jpeg','ceritaku_hari_ini_20180208100644','2018-02-08 10:06:46','0','1'),(4,9,11,20,'1','asasasas','<a href=\"http://nicLinkButton\" title=\"nicLinkButton\" target=\"\">javascript:nicTemp();</a>&nbsp;<br>','','/uploads/2018/February/thumbnails/article_thumbnail/asasasas_20180208113419_68213_thumb.jpeg','asasasas_20180208113419','2018-02-08 11:34:19','0','1'),(10,9,16,62,'2','Project_12-20_Full HD.mp4','Project_12-20_Full HD.mp4                                    ','','/uploads/2018/February/thumbnails/article_thumbnail/project_12_20_full_hd_mp4_20180208133033_16452_thumb.jpeg','project_12_20_full_hd_mp4_20180208133033','2018-02-08 13:30:33','1','1'),(11,9,11,20,'2','Project_12-20_Full HD.mp4','Project_12-20_Full HD.mp4                                    ','','/uploads/2018/February/thumbnails/article_thumbnail/project_12_20_full_hd_mp4_20180208134406_66006_thumb.jpeg','project_12_20_full_hd_mp4_20180208134406','2018-02-08 13:44:07','1','1');

/*Table structure for table `email_tb` */

DROP TABLE IF EXISTS `email_tb`;

CREATE TABLE `email_tb` (
  `id_email` int(11) NOT NULL AUTO_INCREMENT,
  `send_to` varchar(250) DEFAULT NULL,
  `mail_subject` text,
  `mail_message` text,
  `send_datetime` datetime DEFAULT NULL,
  `send_status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_email`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `email_tb` */

insert  into `email_tb`(`id_email`,`send_to`,`mail_subject`,`mail_message`,`send_datetime`,`send_status`) values (12,'limitcodegame@gmail.com','Registation Account','<h1>Email Verification</h1>\r\n									<a href=http://localhost/portal-blog/index.php/verify/a7f3bc4671986e10ae9cf12f4aa8880b>Verify</a>','2018-02-05 12:10:55','sended'),(13,'jati.prasetyo@mobiwin.co.id','Registation Account','<h1>Email Verification</h1>\r\n									<a href=http://192.168.1.111/portal-blog/index.php/verify/4baf413adb608259bd413338b0e116fd>Verify</a>','2018-02-08 09:49:10','sended');

/*Table structure for table `login_log_tb` */

DROP TABLE IF EXISTS `login_log_tb`;

CREATE TABLE `login_log_tb` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `ip_user` varchar(100) DEFAULT NULL,
  `browser_info` text,
  `datetime_login` datetime DEFAULT NULL,
  PRIMARY KEY (`id_log`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

/*Data for the table `login_log_tb` */

insert  into `login_log_tb`(`id_log`,`id_user`,`ip_user`,`browser_info`,`datetime_login`) values (1,5,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-24 11:03:14'),(2,5,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-24 11:13:32'),(3,5,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-24 11:41:06'),(4,5,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-24 15:16:57'),(5,5,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-24 16:26:56'),(6,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-25 10:06:21'),(7,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-25 10:48:01'),(8,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-25 15:53:17'),(9,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-26 09:12:22'),(10,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-26 15:31:45'),(11,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-29 09:37:54'),(12,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-29 14:36:37'),(13,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-29 15:07:22'),(14,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-30 09:22:51'),(15,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-30 10:33:04'),(16,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-30 10:50:25'),(17,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-30 10:51:50'),(18,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-30 10:52:09'),(19,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-30 10:52:35'),(20,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-30 11:05:19'),(21,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-30 11:05:57'),(22,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-30 11:30:53'),(23,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-30 11:31:05'),(24,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-30 11:54:52'),(25,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-30 11:55:36'),(26,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-30 11:56:01'),(27,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-30 12:04:37'),(28,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-30 12:05:19'),(29,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-30 12:34:34'),(30,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-30 12:55:32'),(31,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-30 13:35:50'),(32,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-31 14:53:06'),(33,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-02-01 09:23:50'),(34,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-02-05 10:46:53'),(35,19,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-02-05 12:54:33'),(36,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-02-05 12:56:15'),(37,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-02-05 15:50:33'),(38,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-02-06 09:17:45'),(39,19,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-02-07 16:00:37'),(40,19,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-02-07 16:29:24'),(41,19,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-02-08 09:32:43'),(42,19,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-02-08 09:33:42'),(43,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-02-08 09:39:47'),(44,20,'192.168.1.112','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','2018-02-08 09:55:21'),(45,9,'192.168.1.111','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-02-08 10:08:32'),(46,20,'192.168.1.101','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','2018-02-08 13:00:08'),(47,9,'192.168.1.102','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-02-08 13:02:30'),(48,9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-02-09 10:20:43');

/*Table structure for table `menu_sub_tb` */

DROP TABLE IF EXISTS `menu_sub_tb`;

CREATE TABLE `menu_sub_tb` (
  `id_sub` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` int(11) DEFAULT NULL,
  `sub_name` varchar(30) DEFAULT NULL,
  `sub_url` text,
  `sub_status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_sub`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=latin1;

/*Data for the table `menu_sub_tb` */

insert  into `menu_sub_tb`(`id_sub`,`id_menu`,`sub_name`,`sub_url`,`sub_status`) values (20,11,'Bencana','bencana','1'),(21,11,'Kecelakaan','kecelakaan','1'),(22,11,'Adat','adat','1'),(23,11,'Kehidupan Masyarakat','kehidupan-masyarakat','1'),(24,11,'Lingkungan','lingkungan','1'),(25,11,'Kriminalitas','kriminalitas','1'),(26,12,'Anime','anime','1'),(27,12,'Pertunjukan','pertunjukan','1'),(28,12,'Selebriti','selebriti','1'),(29,12,'TV Show','tv-show','1'),(30,12,'Sinetron','sinetron','1'),(31,12,'Musik','musik','1'),(32,12,'Film','film','1'),(33,13,'Keuangan','keuangan','1'),(34,13,'Industri','industri','1'),(35,13,'Peluang Usaha','peluang-usaha','1'),(36,13,'UMKM','umkm','1'),(37,13,'Investasi','investasi','1'),(38,13,'Finansial','finansial','1'),(39,18,'CPNS','cpns','1'),(40,12,'Unik','unik','1'),(41,12,'Humor','humor','1'),(42,16,'Sains','sains','1'),(43,13,'Pemasaran','pemasaran','1'),(46,13,'Pajak','pajak','1'),(47,13,'Properti','properti','1'),(49,13,'Perbankan','perbankan','1'),(50,13,'Startup','startup','1'),(51,13,'Periklanan','periklanan','1'),(53,14,'MotoGP','motogp','1'),(54,14,'Sepak Bola','sepak-bola','1'),(55,14,'Bulutangkis','bulutangkis','1'),(56,16,'Produk Digital','produk-digital','1'),(57,16,'Games','games','1'),(58,16,'Industri Komunikasi','industri-komunikasi','1'),(59,16,'Software','software','1'),(60,16,'Internet','internet','1'),(61,16,'Hardware','hardware','1'),(62,16,'Ponsel','ponsel','1'),(63,17,'Beasiswa','beasiswa','1'),(64,17,'Sekolah Dasar','sekolah-dasar','1'),(65,17,'Sekolah Menengah Pertama','sekolah-menengah-pertama','1'),(66,17,'Sekolah Menengah Atas','sekolah-menengah-atas','1'),(67,17,'Sekolah Menengah Kejuruan','sekolah-menengah-kejuruan','1'),(68,17,'Perguruan Tinggi','perguruan-tinggi','1'),(69,17,'Pasca Sarjana','pasca-sarjana','1'),(70,17,'Doktoral','doktoral','1'),(71,18,'Teknologi','teknologi','1'),(72,18,'Polisi','polisi','1'),(73,18,'Guru','guru','1'),(74,17,'Pertahanan','pertahanan','1'),(75,18,'Bank','bank','1'),(76,18,'Pemerintahan','pemerintahan','1'),(77,19,'Hobby','hobby','1'),(78,19,'Hewan Peliharaan','hewan-peliharaan','1'),(79,19,'Mode','mode','1'),(80,19,'Parenting','parenting','1'),(81,19,'Kesehatan','kesehatan','1'),(82,19,'Traveling','traveling','1'),(83,19,'Percintaan','percintaan','1'),(84,19,'Rumah Tangga','rumah-tangga','1'),(85,19,'Makanan','makanan','1'),(86,20,'Sepeda Motor','sepeda-motor','1'),(87,20,'Mobil','mobil','1');

/*Table structure for table `menus_tb` */

DROP TABLE IF EXISTS `menus_tb`;

CREATE TABLE `menus_tb` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(30) DEFAULT NULL,
  `menu_url` text,
  `menu_status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `menus_tb` */

insert  into `menus_tb`(`id_menu`,`menu_name`,`menu_url`,`menu_status`) values (11,'Sosial','sosial','1'),(12,'Hiburan','hiburan','1'),(13,'Ekonomi','ekonomi','1'),(14,'Olahraga','olahraga','1'),(16,'Teknologi','teknologi','1'),(17,'Edukasi','edukasi','1'),(18,'Karir','karir','1'),(19,'Gaya Hidup','gaya-hidup','1'),(20,'Otomotif','otomotif','1');

/*Table structure for table `notif_tb` */

DROP TABLE IF EXISTS `notif_tb`;

CREATE TABLE `notif_tb` (
  `id_notif` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `kind_notif` varchar(100) DEFAULT NULL,
  `url_notif` text,
  `status_notif` int(11) DEFAULT NULL,
  `datetime_notif` datetime DEFAULT NULL,
  PRIMARY KEY (`id_notif`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `notif_tb` */

insert  into `notif_tb`(`id_notif`,`id_user`,`kind_notif`,`url_notif`,`status_notif`,`datetime_notif`) values (1,9,'Your Article Published','article/preview/4',1,'2018-02-08 13:48:31'),(2,9,'Your Article Published','video/preview/10',1,'2018-02-08 14:12:19'),(3,9,'Someone Comment','article/preview/2',1,'2018-02-08 14:19:56'),(4,9,'Someone Comment','article/preview/2',1,'2018-02-08 16:28:25'),(5,9,'Someone Comment','article/preview/2',1,'2018-02-08 16:29:13'),(6,9,'Someone Comment','article/preview/4',1,'2018-02-08 17:10:54');

/*Table structure for table `tags_tb` */

DROP TABLE IF EXISTS `tags_tb`;

CREATE TABLE `tags_tb` (
  `id_tag` int(11) NOT NULL AUTO_INCREMENT,
  `content_link` text,
  `the_tags` text,
  `tag_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id_tag`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `tags_tb` */

insert  into `tags_tb`(`id_tag`,`content_link`,`the_tags`,`tag_datetime`) values (1,'lazio_gagal_perlebar_jarak__juventus_dan_napoli_bersiang_ketat__update_klasemen_serie_a_20180206110738','serie a,napoli,lazio,juventus','2018-02-06 11:07:38'),(2,'akhir_perjuangan_dianti_20180206111329','tanah longsor','2018-02-06 11:13:32'),(3,'ceritaku_hari_ini_20180208100644','cerita, curhat','2018-02-08 10:06:47'),(4,'asasasas_20180208113419','asasas','2018-02-08 11:34:19'),(5,'mengirim_pesan_kepada_beberapa_kontak_di_whatsapp_pada_android_20180208120828','Whatsapp','2018-02-08 12:08:28');

/*Table structure for table `user_reqpass_tb` */

DROP TABLE IF EXISTS `user_reqpass_tb`;

CREATE TABLE `user_reqpass_tb` (
  `id_req` int(11) NOT NULL AUTO_INCREMENT,
  `email_user` varchar(200) DEFAULT NULL,
  `code` varchar(30) DEFAULT NULL,
  `request_date` varchar(30) DEFAULT NULL,
  `end_date` varchar(30) DEFAULT NULL,
  `req_verify` varchar(10) DEFAULT NULL,
  `req_verify_date` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_req`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_reqpass_tb` */

/*Table structure for table `user_verify_tb` */

DROP TABLE IF EXISTS `user_verify_tb`;

CREATE TABLE `user_verify_tb` (
  `id_verify` int(11) NOT NULL AUTO_INCREMENT,
  `email_user` varchar(200) DEFAULT NULL,
  `verify_code` varchar(200) DEFAULT NULL,
  `register_date` varchar(30) DEFAULT NULL,
  `valid_date` varchar(30) DEFAULT NULL,
  `verify_date` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_verify`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

/*Data for the table `user_verify_tb` */

insert  into `user_verify_tb`(`id_verify`,`email_user`,`verify_code`,`register_date`,`valid_date`,`verify_date`) values (20,'limitcodegame@gmail.com','a7f3bc4671986e10ae9cf12f4aa8880b','2018-02-05 12:10:52','2018-02-05 12:40:52','2018-02-06 17:24:51'),(21,'jati.prasetyo@mobiwin.co.id','4baf413adb608259bd413338b0e116fd','2018-02-08 09:49:06','2018-02-08 10:19:06','2018-02-08 09:56:57');

/*Table structure for table `users_tb` */

DROP TABLE IF EXISTS `users_tb`;

CREATE TABLE `users_tb` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `email_user` varchar(200) DEFAULT NULL,
  `password_user` varchar(200) DEFAULT NULL,
  `name_user` varchar(200) DEFAULT NULL,
  `bhirtday_user` varchar(30) DEFAULT NULL,
  `gender_user` varchar(10) DEFAULT NULL,
  `photo_user` text,
  `auth_by` varchar(20) DEFAULT NULL,
  `register_date` varchar(30) DEFAULT NULL,
  `verify` varchar(10) DEFAULT NULL,
  `user_status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `users_tb` */

insert  into `users_tb`(`id_user`,`email_user`,`password_user`,`name_user`,`bhirtday_user`,`gender_user`,`photo_user`,`auth_by`,`register_date`,`verify`,`user_status`) values (9,'juri@pebrianto.com','5f4dcc3b5aa765d61d8327deb882cf99','Juri Pebrianto','1921-02-2','male','','self','2018-01-25 10:01:02','1','3'),(19,'limitcodegame@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','Limit Code','1920-01-1','male','','self','2018-02-05 12:10:52','1','1'),(20,'jati.prasetyo@mobiwin.co.id','1cf97d1083eb7e009bf23a310f4113e3','Jati Ganteng','1993-07-17','male','','self','2018-02-08 09:49:06','1','1');

/*Table structure for table `visitor_tb` */

DROP TABLE IF EXISTS `visitor_tb`;

CREATE TABLE `visitor_tb` (
  `id_visitor` int(11) NOT NULL AUTO_INCREMENT,
  `ip_visitor` varchar(100) DEFAULT NULL,
  `browser_info` text,
  `datetime_visit` datetime DEFAULT NULL,
  PRIMARY KEY (`id_visitor`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `visitor_tb` */

insert  into `visitor_tb`(`id_visitor`,`ip_visitor`,`browser_info`,`datetime_visit`) values (2,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-18 10:27:38'),(3,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-18 10:27:53'),(4,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-19 13:39:43'),(5,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-19 13:39:44'),(6,'192.168.1.115','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-19 13:41:58'),(7,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-24 13:57:01'),(8,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-24 14:03:42'),(9,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-01-30 15:27:38'),(10,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-02-01 11:33:04'),(11,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-02-01 11:38:21'),(12,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-02-01 11:40:58'),(13,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-02-01 11:42:05'),(14,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-02-01 11:42:36'),(15,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-02-01 11:43:02'),(16,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-02-01 11:43:30'),(17,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-02-01 11:43:42'),(18,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36','2018-02-01 12:53:03');

/*Table structure for table `watch_content_tb` */

DROP TABLE IF EXISTS `watch_content_tb`;

CREATE TABLE `watch_content_tb` (
  `id_watch` int(11) NOT NULL AUTO_INCREMENT,
  `id_content` int(11) DEFAULT NULL,
  `ip_client` varchar(200) DEFAULT NULL,
  `watch_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id_watch`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

/*Data for the table `watch_content_tb` */

insert  into `watch_content_tb`(`id_watch`,`id_content`,`ip_client`,`watch_datetime`) values (56,3,'::1','2018-02-12 13:24:24'),(57,3,'::1','2018-02-12 13:24:59'),(58,3,'::1','2018-02-12 13:26:23'),(59,3,'::1','2018-02-12 14:57:58');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
